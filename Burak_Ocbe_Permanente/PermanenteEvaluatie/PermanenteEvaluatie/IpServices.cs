﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PermanenteEvaluatie
{
    public class IpServices
    {
        private readonly IIpInterface ipInterface;
        public IpServices(IIpInterface ipInterface)
        {
            this.ipInterface = ipInterface;
        }

        public string GetTimeZone(string ip)
        {
            var time = ipInterface.GetTime(ip);
            DateTime timeZone = DateTime.Parse(time);
            if (timeZone.Hour > 0  && timeZone.Hour < 7)
            {
                return "Op dit IP is het momenteel nacht.";
            }
            if (timeZone.Hour > 7 && timeZone.Hour < 12)
            {
                return "Op dit IP is het momenteel voormiddag.";
            }
            if (timeZone.Hour > 12 && timeZone.Hour < 13)
            {
                return "Op dit IP is het momenteel middag.";
            }
            if (timeZone.Hour > 13 && timeZone.Hour < 17)
            {
                return "Op dit IP is het momenteel namiddag.";
            }
            if (timeZone.Hour > 17)
            {
                return "Op dit IP is het momenteel avond.";
            }
            return "Fout";
        }
    }
}
