﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PermanenteEvaluatie
{
    public class IpApi : IIpInterface
    {
        public string GetTime(string ip)
        {
            string url = $"http://worldtimeapi.org/api/{ip}";
            using (var httpClient = new HttpClient())
            {
                var httpRespone = httpClient.GetAsync(url).GetAwaiter().GetResult();
                var response = httpRespone.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                return JsonConvert.DeserializeObject<IpProperties>(response).TimeZone.ToString();
            }
        }
    }
}
