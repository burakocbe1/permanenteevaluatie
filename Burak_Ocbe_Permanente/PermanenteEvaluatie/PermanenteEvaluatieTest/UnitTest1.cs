using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using PermanenteEvaluatie;

namespace PermanenteEvaluatieTest
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void GetTime_Returns_nacht()
        {
            //Arrange
            var ipApi = new Mock<IIpInterface>();
            ipApi.Setup(x => x.GetTime("195.130.131.38")).Returns();

            //Act
            var ipServices = new IpServices(ipApi.Object);

            //Assert
            Assert.AreEqual("Op dit IP is het momenteel nacht.", ipServices.GetTimeZone("195.130.131.38"));
        }
    }
}
